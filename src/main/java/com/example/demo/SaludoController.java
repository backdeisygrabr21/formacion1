package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaludoController {
    @RequestMapping("/saludo")
    public String index(){
        return "Hola, yo soy tu API";
    }
}
